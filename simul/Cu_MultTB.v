module TB_CU();
  reg clk, rst, A0,start;
  wire ldA, ldB, ldP, zero, shen, ready;
  wire [2:0] S0;
  Cu_Mult CU ( rst, clk,  A0, start, ldA, ldB, ldP, zero, shen, ready, S0);
  initial begin
    clk=0;
    A0=0;
    repeat (200)
   #150 clk=~clk;
  
   
    end
    initial begin
    #10 rst=1;
    #20 rst=0;
     //s=0;
     start = 0;
     #380
     start=1;
     #100;
     start=0;
     #1200;
     A0=1;
     #300;
     A0=0;
     #300;
     A0=1;
     #300;
     A0=0;
     #1400;
     start=1;
     #100;
     start=0;
     #6000
   
   $stop;
   end
  
  
  
endmodule