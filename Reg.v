module Reg (input [7:0]A, input clk, rst, zero, ldA, output reg[7:0] B);

 always@(posedge clk, posedge rst, posedge zero)begin
  if (rst) B = 8'b00000000;
  else  
   if(zero) B = 8'b00000000;
    else if(ldA) B <= A;
 end

endmodule