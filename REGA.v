module REGA(input [7:0] ABus , input clk , rst , lda, shen, si ,output reg [7:0] AOut, output so);
	always @(posedge clk , posedge rst) begin
		if(rst) AOut <= 8'b0;
		else if(lda) AOut <= ABus;
		else if(shen) AOut <= {si , ABus[7:1]};
	end
	assign so = AOut[0];
endmodule