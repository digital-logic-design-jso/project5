module Cu_Mult (input rst, clk,  A0, start, output reg ldA, ldB, ldP, zero, shen, ready, output reg [2:0] S0);
  parameter [1:0]idle=0, init=1, loading=2, MULT=3;
  reg [1:0] ns, ps;
  reg [3:0] cnt;
  reg cmp=0,en=0;
  always @(posedge clk, posedge rst) begin
    {ldA, ldB, ldP, zero, shen, ready, S0} = 7'b0;
    
    case (ps)
      idle: begin  if(start) ns = init;
                    else ns = idle;
            ready = 1; en = 0;end
      init : begin if(start) ns = init;
                   else ns = loading;
                   zero = 1;
                   S0 = 1;
                 end
      loading : begin
                  ldA = 1; ldB = 1; ns = MULT; en = 1;
                end
      MULT : begin ldP = 1; shen = 1; S0 = {2'b00 , ~A0};
              if(cmp)begin ns = idle; en = 0; end
              else ns = MULT;
              end
    endcase
  end
         
  always @(posedge clk, posedge rst) begin
    if (rst) ps = 4'b0;
    else ps = ns;
  end
  
   always @(posedge clk)begin // counter 
    if(en)begin
      if(cnt == 4'b0110)begin
        cmp <= 1; cnt <= 0; end
      else
        cnt <= cnt + 1;
      end
    else
     begin cnt <= 0; cmp = 0; end
  end
  
  
endmodule